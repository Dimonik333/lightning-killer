﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(CurveRangeAttribute))]
public class AnimationCurveDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var range = attribute as CurveRangeAttribute;
        if (property.propertyType == SerializedPropertyType.AnimationCurve)
        {
            var curve = EditorGUI.CurveField(position, label, property.animationCurveValue, Color.green, range.GetRect());
            property.animationCurveValue = curve;
        }
        else
            EditorGUI.LabelField(position, label.text, "Use CurveRange with AnimationCurve.");
    }
}