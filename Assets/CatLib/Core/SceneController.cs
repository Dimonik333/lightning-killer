﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController
{
    private Scene _scene;
    private string _sceneName;

    public SceneController(string sceneName)
    {
        _sceneName = sceneName;
    }
    public Scene GetScene()
    {
        return _scene;
    }

    public void CreateScene()
    {
        var scene = SceneManager.GetSceneByName(_sceneName);
        if (scene.isLoaded)
            _scene = scene;
        else
            _scene = SceneManager.CreateScene(_sceneName);
    }

    public void UnloadScene()
    {
        //_scene = SceneManager.GetSceneByName(_sceneName);
        if (_scene.isLoaded)
        {
            _scene.name += "_Delete";
            SceneManager.UnloadSceneAsync(_scene);
        }
    }

    public void MoveGameObjectToScene(GameObject instance)
    {
        SceneManager.MoveGameObjectToScene(instance, _scene);
    }

    public void MoveGameObjectToScene<T>(T component) where T : MonoBehaviour
    {
        SceneManager.MoveGameObjectToScene(component.gameObject, _scene);
    }
}