﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Выполняет линейное изменение значения в диапазоне ( 0 - 1 ) за указанное время
/// </summary>
public class ProgressTimeRoutine
{
    private uint? _coroutineId;

    private const float MinValue = 0f;
    private const float MaxValue = 1f;

    private float _value;

    private float _frontDuration;
    private float _backDuration;

    private ProgressRoutineState _state;
    private Action<float> _progressAction;

    private Func<float> _getDeltaTimeStrategy;
    private YieldInstruction _yieldInstruction = null;

    public event Action<ProgressTimeRoutineStateChangedArgs> StateChanged;
    public ProgressRoutineState State
    {
        get { return _state; }
        protected set
        {
            var lastState = _state;
            _state = value;

            if (_state != lastState)
                StateChanged?.Invoke(new ProgressTimeRoutineStateChangedArgs(this, lastState, _state));
        }
    }

    public float TimeMultiplier { get; set; } = 1f;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="startValue"> Стартовое значение. От 0 до 1.</param>
    /// <param name="frontDuration"> Время, требуемое на полный проход от 0 до 1. </param>
    /// <param name="backDuration"> Время, требуемое на обратный проход от 1 до 0. </param>
    /// <param name="unscaledTime"> Будет ли использоваться немашатбируемое время. </param>
    /// <param name="deltaTimeType"> Тип временного интервала. Обычный или физика. </param>
    /// <param name="progressAction"> Действие, выполняемое при изменении значения. </param>
    public ProgressTimeRoutine(float startValue, float frontDuration, float backDuration, bool unscaledTime, DeltaTimeType deltaTimeType, Action<float> progressAction)
    {
        _value = Mathf.Clamp(startValue, MinValue, MaxValue);

        _frontDuration = frontDuration;
        _backDuration = backDuration;
        _progressAction = progressAction;

        _getDeltaTimeStrategy = TimeFactory.GetTimeStrategy(deltaTimeType, unscaledTime);
        _yieldInstruction = TimeFactory.GetYieldInstruction(deltaTimeType);

        State = ProgressRoutineState.Stop;
    }

    /// <summary>
    /// Запускает процесс изменения значения от текущего к 1
    /// </summary>
    public void Front(Action onCompleted = null)
    {
        var duration = _frontDuration * (MaxValue - _value) / (MaxValue - MinValue);
        StopProgressCoroutine();
        State = ProgressRoutineState.Front;
        StartProgressCoroutine(MaxValue, duration, onCompleted);
    }

    /// <summary>
    /// Запускает процесс изменения значения от текущего к 0
    /// </summary>
    public void Back(Action onCompleted = null)
    {
        var duration = _backDuration * (_value - MinValue) / (MaxValue - MinValue);
        StopProgressCoroutine();
        State = ProgressRoutineState.Back;
        StartProgressCoroutine(MinValue, duration, onCompleted);
    }

    /// <summary>
    /// Запускает процесс изменения значения от текущего к указанному
    /// </summary>
    public void ToValue(float value, Action onCompleted = null)
    {
        var duration = 0f;
        var state = ProgressRoutineState.Stop;

        if (value > _value)
        {
            duration = _frontDuration * (_value - value) / (MaxValue - MinValue);
            state = ProgressRoutineState.Front;
        }
        else
        {
            duration = _backDuration * (_value - value) / (MaxValue - MinValue);
            state = ProgressRoutineState.Back;
        }

        StopProgressCoroutine();
        State = state;
        StartProgressCoroutine(value, duration, onCompleted);
    }


    /// <summary>
    /// Остановить выполнение и установить значение в начало - 0
    /// </summary>
    public void SetStart()
    {
        Stop();
        _value = MinValue;
        _progressAction(_value);
    }

    /// <summary>
    /// Остановить выполнение и установить значение в конец - 1
    /// </summary>
    public void SetEnd()
    {
        Stop();
        _value = MaxValue;
        _progressAction(_value);
    }

    /// <summary>
    /// Останавливает выполнение и устанавливает в указанное значение
    /// </summary>
    public void SetValue(float value)
    {
        Stop();
        _value = value;
        _progressAction(_value);
    }

    public void Stop()
    {
        State = ProgressRoutineState.Stop;
        StopProgressCoroutine();
    }



    private void StartProgressCoroutine(float targetValue, float duration, Action onCompleted)
    {
        _coroutineId = CoroutineManager.Start(ProgressCoroutine(_value, targetValue, duration, () => { State = ProgressRoutineState.Stop; onCompleted?.Invoke(); }));
    }

    private void StopProgressCoroutine()
    {
        CoroutineManager.Stop(_coroutineId);
    }

    private IEnumerator ProgressCoroutine(float startValue, float targetValue, float duration, Action onCompleted)
    {
        var progress = 0f;
        var elapsedTime = 0f;

        while (elapsedTime < duration)
        {
            progress = elapsedTime / duration;
            _value = Mathf.Lerp(startValue, targetValue, progress);
            _progressAction(_value);

            elapsedTime += _getDeltaTimeStrategy() * TimeMultiplier;
            yield return _yieldInstruction;
        }
        _value = targetValue;
        _progressAction(_value);
        onCompleted?.Invoke();
    }
}

public static class TimeFactory
{
    public static float GetDeltaTime() { return Time.deltaTime; }
    public static float GetFixedDeltaTime() { return Time.deltaTime; }
    public static float GetUnscaledDeltaTime() { return Time.unscaledDeltaTime; }
    public static float GetUnscaledFixedDeltaTime() { return Time.fixedUnscaledDeltaTime; }

    public static Func<float> GetTimeStrategy(DeltaTimeType deltaTimeType, bool unscaledTime)
    {
        switch (deltaTimeType)
        {
            case DeltaTimeType.Update:
                if (unscaledTime)
                    return GetUnscaledDeltaTime;
                else
                    return GetDeltaTime;
            case DeltaTimeType.FixedUpdate:
                if (unscaledTime)
                    return GetUnscaledFixedDeltaTime;
                else
                    return GetFixedDeltaTime;
            default:
                return GetDeltaTime;
        }
    }

    public static YieldInstruction GetYieldInstruction(DeltaTimeType deltaTimeType)
    {
        switch (deltaTimeType)
        {
            case DeltaTimeType.Update:
                return null;
            case DeltaTimeType.FixedUpdate:
                return new WaitForFixedUpdate();
            default:
                return null;

        }
    }
}