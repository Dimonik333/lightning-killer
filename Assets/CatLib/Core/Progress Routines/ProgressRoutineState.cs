﻿using UnityEngine;

public enum ProgressRoutineState
{
    Front,
    Back,
    Stop
}