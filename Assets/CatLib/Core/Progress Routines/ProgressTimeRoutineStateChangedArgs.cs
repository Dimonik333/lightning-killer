﻿public class ProgressTimeRoutineStateChangedArgs
{
    public ProgressTimeRoutineStateChangedArgs(ProgressTimeRoutine routine, ProgressRoutineState lastState, ProgressRoutineState currentState)
    {
        Routine = routine;
        LastState = lastState;
        CurrentState = currentState;
    }
    public ProgressTimeRoutine Routine;
    public ProgressRoutineState CurrentState;
    public ProgressRoutineState LastState;
}