﻿using System;
using System.Collections;
using UnityEngine;

public class LoopTimeRoutine : MonoBehaviour
{
    private uint? _coroutineId;
    private bool _play;

    private const float MinValue = 0f;
    private const float MaxValue = 1f;
    private float _value;
    private float _duration;
    private LoopRoutineDirection _direction;
    private Action<float> _progressAction;


    private Func<float> _getDeltaTimeStrategy;
    private YieldInstruction _yieldInstruction = null;

    public float TimeMultiplier { get; set; } = 1f;
    public event Action<LoopTimeRoutine, LoopRoutineDirection> DirectionChanged;
    public LoopRoutineDirection Direction
    {
        get { return _direction; }
        set
        {
            _direction = value;
            DirectionChanged?.Invoke(this, _direction);
        }
    }

    public LoopTimeRoutine(float startValue, float duration, LoopRoutineDirection direction, bool unscaledTime, DeltaTimeType deltaTimeType, Action<float> progressAction)
    {
        _value = startValue;
        _duration = duration;
        _progressAction = progressAction;
        _play = false;
        _direction = direction;

        _getDeltaTimeStrategy = TimeFactory.GetTimeStrategy(deltaTimeType, unscaledTime);
        _yieldInstruction = TimeFactory.GetYieldInstruction(deltaTimeType);
    }


    public void Play()
    {
        _play = true;
        CoroutineManager.Stop(_coroutineId);
        _coroutineId = CoroutineManager.Start(PingPongProgressRoutine(_duration));
    }

    public void Stop()
    {
        _play = false;
    }

    public void Break()
    {
        _play = false;
        CoroutineManager.Stop(_coroutineId);
    }

    private IEnumerator PingPongProgressRoutine(float duration)
    {
        var startValue = MinValue;
        var endValue = MaxValue;

        switch (Direction)
        {
            case LoopRoutineDirection.Front:
                startValue = MinValue;
                endValue = MaxValue;
                break;
            case LoopRoutineDirection.Back:
                startValue = MaxValue;
                endValue = MinValue;
                break;
        }

        var progress = Mathf.Abs((endValue - _value) / (endValue - startValue));
        var elapsedTime = duration * progress;

        while (_play)
        {
            while (elapsedTime < duration)
            {
                progress = elapsedTime / duration;
                _value = Mathf.Lerp(startValue, endValue, progress);
                _progressAction(_value);

                elapsedTime += _getDeltaTimeStrategy() * TimeMultiplier;
                yield return _yieldInstruction;
            }
            elapsedTime -= duration;

            switch (Direction)
            {
                case LoopRoutineDirection.Front:
                    startValue = MaxValue;
                    endValue = MinValue;
                    Direction = LoopRoutineDirection.Back;
                    break;
                case LoopRoutineDirection.Back:
                    startValue = MinValue;
                    endValue = MaxValue;
                    Direction = LoopRoutineDirection.Front;
                    break;
            }
        }
    }
}

public enum LoopRoutineDirection
{
    Front,
    Back
}


public class LoopTimeRoutineNew : MonoBehaviour
{
    private uint? _coroutineId;
    private bool _play;

    private const float MinValue = 0f;
    private const float MaxValue = 1f;
    private float _value;
    private float _duration;
    private LoopRoutineDirection _direction;
    private ProgressRoutineAction _routineAction;

    private Func<float> _getDeltaTimeStrategy;
    private YieldInstruction _yieldInstruction = null;

    public float TimeMultiplier { get; set; } = 1f;
    public event Action<LoopTimeRoutineNew, LoopRoutineDirection> DirectionChanged;
    public LoopRoutineDirection Direction
    {
        get { return _direction; }
        set
        {
            _direction = value;
            DirectionChanged?.Invoke(this, _direction);
        }
    }

    public LoopTimeRoutineNew(float startValue, float duration, LoopRoutineDirection direction, bool unscaledTime, DeltaTimeType deltaTimeType, ProgressRoutineAction action)
    {
        _value = startValue;
        _duration = duration;
        _routineAction = action;
        _play = false;
        _direction = direction;

        _getDeltaTimeStrategy = TimeFactory.GetTimeStrategy(deltaTimeType, unscaledTime);
        _yieldInstruction = TimeFactory.GetYieldInstruction(deltaTimeType);
    }

    public void Play()
    {
        _play = true;
        _routineAction.OnPlay();
        CoroutineManager.Stop(_coroutineId);
        _coroutineId = CoroutineManager.Start(PingPongProgressRoutine(_duration));
    }

    public void Stop()
    {
        _play = false;
        _routineAction.OnStop();
    }

    public void Break()
    {
        _play = false;
        CoroutineManager.Stop(_coroutineId);
    }

    private IEnumerator PingPongProgressRoutine(float duration)
    {
        var startValue = MinValue;
        var endValue = MaxValue;

        switch (Direction)
        {
            case LoopRoutineDirection.Front:
                startValue = MinValue;
                endValue = MaxValue;
                break;
            case LoopRoutineDirection.Back:
                startValue = MaxValue;
                endValue = MinValue;
                break;
        }

        var progress = Mathf.Abs((endValue - _value) / (endValue - startValue));
        var elapsedTime = duration * progress;

        while (_play)
        {
            while (elapsedTime < duration)
            {
                progress = elapsedTime / duration;
                _value = Mathf.Lerp(startValue, endValue, progress);
                _routineAction.OnProgress(_value);

                elapsedTime += _getDeltaTimeStrategy() * TimeMultiplier;
                yield return _yieldInstruction;
            }
            elapsedTime -= duration;

            switch (Direction)
            {
                case LoopRoutineDirection.Front:
                    startValue = MaxValue;
                    endValue = MinValue;
                    Direction = LoopRoutineDirection.Back;
                    break;
                case LoopRoutineDirection.Back:
                    startValue = MinValue;
                    endValue = MaxValue;
                    Direction = LoopRoutineDirection.Front;
                    break;
            }
        }
    }
}

public abstract class ProgressRoutineAction
{
    public abstract void OnPlay();
    public abstract void OnStop();
    public abstract void OnProgress(float t);
}

public class TurnRoutineAction : ProgressRoutineAction
{
    private Rigidbody2D _rigidbody;
    private AnimationCurve _turnCurve;

    public override void OnPlay()
    {
    }

    public override void OnProgress(float t)
    {
        var angle = Mathf.Lerp(-90, 90, t);
        _rigidbody.rotation = angle;
    }

    public override void OnStop()
    {
    }
}