﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Управляет корутинами. 
/// Полезен для мест, где не используются MonoBehaviour
/// </summary>
public class CoroutineManager : MonoSingleton<CoroutineManager>
{
    private Dictionary<uint, Coroutine> _coroutines = new Dictionary<uint, Coroutine>();
    private uint _coroutineId = 0;

    public static int Count => Instance._coroutines.Count;

    private void OnEnable()
    {
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    private void OnDisable()
    {
        StopAllManagedCoroutines();
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }


    private void OnSceneUnloaded(Scene scene)
    {
        Instance.StopAllManagedCoroutines();
    }

    public static uint? Delay(float delay, bool unscaled, Action callback)
    {
        return Instance.SetDelayCoroutine(delay, unscaled, callback);
    }

    public static uint? Start(IEnumerator en)
    {
        return Instance.SetCoroutine(en);
    }

    public static bool Has(uint? coroutineId)
    {
        return Instance.HasCoroutine(coroutineId);
    }

    public static void Stop(uint? coroutineId)
    {
        Instance.StopManagedCoroutine(coroutineId);
    }



    public uint? SetDelayCoroutine(float delay, bool unscaled, Action callback)
    {
        return unscaled
            ? SetCoroutine(UnscaledDelayCoroutine(delay, callback))
            : SetCoroutine(ScaledDelayCoroutine(delay, callback));
    }

    public uint? SetCoroutine(IEnumerator en)
    {
        var coroutineId = GetCoroutineId();
        var coroutine = StartCoroutine(CoroutineBox(coroutineId, en));
        _coroutines.Add(_coroutineId, coroutine);
        return coroutineId;
    }

    public bool HasCoroutine(uint? coroutineId)
    {
        if (!coroutineId.HasValue)
            return false;
        return _coroutines.ContainsKey(coroutineId.Value);
    }

    public void StopManagedCoroutine(uint? coroutineId)
    {
        if (!coroutineId.HasValue || !_coroutines.ContainsKey(coroutineId.Value))
            return;
        var coroutine = _coroutines[coroutineId.Value];
        StopCoroutine(coroutine);
        _coroutines.Remove(coroutineId.Value);
    }



    public void StopAllManagedCoroutines()
    {
        StopAllCoroutines();
        _coroutines.Clear();
    }



    private IEnumerator ScaledDelayCoroutine(float duration, Action callback)
    {
        yield return new WaitForSeconds(duration);
        callback();
    }

    private IEnumerator UnscaledDelayCoroutine(float delay, Action callback)
    {
        yield return new WaitForSeconds(delay);
        callback();
    }

    private IEnumerator CoroutineBox(uint? coroutineId, IEnumerator en)
    {
        yield return en;
        StopManagedCoroutine(coroutineId);
    }

    private uint? GetCoroutineId()
    {
        while (_coroutines.ContainsKey(_coroutineId))
            _coroutineId++;
        return new uint?(_coroutineId);
    }
}