﻿using UnityEngine;

/// <summary>
/// Автоматически создаваемый объект одиночка.
/// При запросе будет создан новый объект.
/// </summary>
public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private bool _initialized = false;
    private static T _instance;

    protected virtual bool DontDestroyOnSceneLoad => true;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                if (_instance == null)
                    _instance = new GameObject(typeof(T).Name, typeof(T)).GetComponent<T>();
                _instance.Initialize();
            }
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance == null)
        {
            _instance = gameObject.GetComponent<T>();
            _instance.Initialize();
        }
        if (_instance != null && _instance != this)
            Destroy(gameObject);
    }

    protected void OnDestroy()
    {
        if (_instance == gameObject)
            _instance = null;
        Uninitialize();
    }
        

    private void Initialize()
    {
        if (_initialized)
            return;
        if (Application.isPlaying && DontDestroyOnSceneLoad)
            DontDestroyOnLoad(_instance.gameObject);
        OnInitialize();
        _initialized = true;
    }


    private void Uninitialize()
    {
        if (!_initialized)
            return;
        OnUninitialize();
        _initialized = false;
    }

    protected virtual void OnUninitialize()
    { }

    protected virtual void OnInitialize()
    { }
}