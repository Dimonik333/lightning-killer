﻿using UnityEngine;

/// <summary>
/// Автоматически создаётся из префаба, хранящегося в Resources/Singletons/$CLASSNAME
/// </summary>
public class ResourceSingleton<T> : MonoBehaviour where T : ResourceSingleton<T>
{
    private bool _initialized = false;
    private static T _instance = null;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                if (_instance == null)
                {
                    var prefab = Resources.Load<T>("Singletons/" + typeof(T).Name);
                    _instance = Instantiate(prefab);
                }
                _instance.Initialize();
            }
            return _instance;
        }
    }

    protected void Awake()
    {
        if (_instance == null)
        {
            _instance = GetComponent<T>();
            _instance.Initialize();
        }
        else if (_instance != null && _instance != this)
            Destroy(gameObject);
    }

    protected void OnDestroy()
    {
        if (_instance == gameObject)
            _instance = null;
        Uninitialize();
    }


    private void Initialize()
    {
        if (!_initialized)
        {
            _initialized = true;
            if (Application.isPlaying)
                DontDestroyOnLoad(_instance.gameObject);
            OnInitialize();
        }
    }

    protected virtual void OnInitialize()
    { }

    private void Uninitialize()
    {
        if (_initialized)
            OnUninitialize();
    }

    protected virtual void OnUninitialize()
    { }
}
