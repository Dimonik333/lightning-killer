﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Создаваемый на сцене вручную объект одиночка.
/// Должен находиться на сцене изначально.
/// </summary>
public class ManagerSingleton<T> : MonoBehaviour where T : ManagerSingleton<T>
{
    private bool _initialized = false;
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                _instance.Initialize();
            }
            return _instance;
        }
    }


    protected virtual void Awake()
    {
        if (_instance == null)
        {
            _instance = gameObject.GetComponent<T>();
            _instance.Initialize();
        }
        if (_instance != null && _instance != this)
            Destroy(gameObject);
    }

    protected void OnDestroy()
    {
        if (_instance == gameObject)
            _instance = null;
        Uninitialize();
    }

    private void Initialize()
    {
        if (_initialized)
            return;

        if (Application.isPlaying)
            DontDestroyOnLoad(_instance.gameObject);
        OnInitialize();
        _initialized = true;

    }

    private void Uninitialize()
    {
        if (!_initialized)
            return;

        OnUninitialize();
        _initialized = false;
    }

    protected virtual void OnInitialize() { }

    protected virtual void OnUninitialize() { }
}
