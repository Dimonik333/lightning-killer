﻿using UnityEngine;

public class CurveRangeAttribute : PropertyAttribute
{
    protected float _startTime;
    protected float _endTime;
    protected float _minValue;
    protected float _maxValue;

    public CurveRangeAttribute(float endTime, float maxValue) : this(0f, endTime, 0f, maxValue)
    { }

    public CurveRangeAttribute(float startTime, float endTime, float minValue, float maxValue)
    {
        _startTime = startTime;
        _endTime = endTime;
        _minValue = minValue;
        _maxValue = maxValue;
    }

    public Rect GetRect()
    {
        var start = new Vector2(_startTime, _minValue);
        var end = new Vector2(_endTime, _maxValue);
        var size = end - start;
        return new Rect(start, size);
    }
}
