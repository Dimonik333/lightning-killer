﻿using System;
using ShowHideComponents;

public class ShowHideGroup
{
    private Action _onComplete;
    private BaseShowHide[] _components;

    public ShowHideGroup(params BaseShowHide[] components)
    {
        _components = components;
    }

    public void Show(Action onComplete = null)
    {
        _onComplete = onComplete;
        for (int i = 0; i < _components.Length; i++)
            if (_components[i].State != ShowHideState.Shown)
                _components[i].Show(CheckShown);
        CheckShown();
    }

    public void Hide(Action onComplete = null)
    {
        _onComplete = onComplete;
        for (int i = 0; i < _components.Length; i++)
            if (_components[i].State != ShowHideState.Hidden)
                _components[i].Hide(CheckHidden);
        CheckHidden();
    }

    public void SetShown()
    {
        for (int i = 0; i < _components.Length; i++)
            _components[i].SetShown();
    }

    public void SetHidden()
    {
        for (int i = 0; i < _components.Length; i++)
            _components[i].SetHidden();
    }

    public void Stop()
    {
        for (int i = 0; i < _components.Length; i++)
            _components[i].Stop();
    }

    private void CheckShown()
    {
        if (CheckState(_components, ShowHideState.Shown))
            _onComplete?.Invoke();
    }

    private void CheckHidden()
    {
        if (CheckState(_components, ShowHideState.Hidden))
            _onComplete?.Invoke();
    }

    private bool CheckState(BaseShowHide[] components, ShowHideState state)
    {
        for (int i = 0; i < components.Length; i++)
            if (components[i].State != state)
                return false;
        return true;
    }

    public void ClearCallback()
    {
        _onComplete = null;
    }
}
