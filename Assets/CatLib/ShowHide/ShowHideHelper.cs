﻿using System.Collections.Generic;

namespace ShowHideComponents
{
    public static class ShowHideHelper
    {
        public static bool CheckShown(params IShowHide[] effects)
        {
            return CheckState(effects, ShowHideState.Shown);
        }

        public static bool CheckHidden(params IShowHide[] effects)
        {
            return CheckState(effects, ShowHideState.Hidden);
        }
        
        private static bool CheckState(IShowHide[] effects, ShowHideState state)
        {
            for(int i = 0; i < effects.Length;i++)            
                if (effects[i].State != state)
                    return false;
            return true;
        }
    }
}