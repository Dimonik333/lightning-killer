﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace ShowHideComponents
{
    public class TrailShowHide : BaseShowHide
    {
        private uint? _delyaRoutineId;

        public TrailRenderer[] Trails;
        public bool Unscaled = false;

        protected override void OnShow(Action onCompleted)
        {
            SetActive(true);
            if (onCompleted != null)
                onCompleted();
        }

        protected override void OnHide(Action onCompleted)
        {
            var maxTime = Trails.Max(t => t.time);
            _delyaRoutineId = CoroutineManager.Delay(maxTime, Unscaled, () =>
            {
                SetActive(false);
                if (onCompleted != null)
                    onCompleted();
            });
        }

        protected override void OnSetShown()
        {
            SetActive(true);
        }

        protected override void OnSetHidden()
        {
            SetActive(false);
        }

        protected override void OnStop()
        {
            CoroutineManager.Stop(_delyaRoutineId);
        }

        private void SetActive(bool value)
        {
            gameObject.SetActive(value);
            foreach (var trail in Trails)
                trail.gameObject.SetActive(value);
        }


    }
}