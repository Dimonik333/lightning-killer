﻿using System;
using UnityEngine;

namespace ShowHideComponents
{
    public delegate void StateChangedHandler(IShowHide showHide, ShowHideState state);

    public interface IShowHide
    {
        ShowHideState State { get; }
        event StateChangedHandler StateChanged;

        void Show(Action onCompleted = null);
        void Hide(Action onCompleted = null);

        void SetShown();
        void SetHidden();

        void Stop();
    }


    //public class ShowHideGroupTemp : IShowHide
    //{
    //    private Action _onCompleted;
    //    private IShowHide[] _components;

    //    private ShowHideState _state;
    //    public ShowHideState State
    //    {
    //        get { return _state; }
    //        protected set
    //        {
    //            if (_state == value)
    //                return;
    //            _state = value;
    //            StateChanged?.Invoke(this, _state);
    //        }
    //    }

    //    public event StateChangedHandler StateChanged;

    //    public ShowHideGroupTemp(params IShowHide[] components)
    //    {
    //        _components = components;
    //    }

    //    public void Show(Action onCompleted = null)
    //    {
    //        _onCompleted = onCompleted;
    //        for (int i = 0; i < _components.Length; i++)
    //            if (_components[i].State != ShowHideState.Shown)
    //                _components[i].Show(CheckShown);
    //    }

    //    public void Hide(Action onCompleted = null)
    //    {
    //        _onCompleted = onCompleted;
    //        for (int i = 0; i < _components.Length; i++)
    //            if (_components[i].State != ShowHideState.Hidden)
    //                _components[i].Hide(CheckHidden);
    //    }

    //    public void SetShown()
    //    {
    //        for (int i = 0; i < _components.Length; i++)
    //            _components[i].SetShown();
    //    }

    //    public void SetHidden()
    //    {
    //        for (int i = 0; i < _components.Length; i++)
    //            _components[i].SetHidden();
    //    }



    //    private void CheckShown()
    //    {
    //        if (ShowHideHelper.CheckShown(_components))
    //            _onCompleted?.Invoke();
    //    }

    //    private void CheckHidden()
    //    {
    //        if (ShowHideHelper.CheckHidden(_components))
    //            _onCompleted?.Invoke();
    //    }
    //}
}