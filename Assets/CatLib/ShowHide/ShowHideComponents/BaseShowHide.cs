﻿using System;
using UnityEngine;

namespace ShowHideComponents
{
    public abstract class BaseShowHide : MonoBehaviour, IShowHide
    {
        private Action _onComplete;

        [SerializeField] private ShowHideState _state;
        [SerializeField] private bool _setOnStart = true;
        [SerializeField] private bool _changeActive = true;

        public ShowHideState State
        {
            get { return _state; }
            protected set
            {
                if (_state == value)
                    return;
                _state = value;
                StateChanged?.Invoke(this, _state);
            }
        }

        public event StateChangedHandler StateChanged;

        private void Start()
        {
            if (_setOnStart && _onComplete == null) // если не null, значит задачу уже запустили раньше
                switch (State)
                {
                    case ShowHideState.Show: Show(); break;
                    case ShowHideState.Shown: SetShown(); break;
                    case ShowHideState.Hide: Hide(); break;
                    case ShowHideState.Hidden: SetHidden(); break;
                }
        }

        private void OnDestroy()
        {
            Stop();
        }

        /// <summary>        
        /// Запускает эффект появления объекта на сцене.
        /// </summary>
        public void Show(Action onComplete = null)
        {
            _onComplete = onComplete;
            switch (State)
            {
                case ShowHideState.Shown:
                    onComplete?.Invoke();
                    break;
                case ShowHideState.Show:
                    break;
                default:
                    SetObjectActive(true);
                    Stop();
                    State = ShowHideState.Show;
                    OnShow(() =>
                    {
                        State = ShowHideState.Shown;
                        _onComplete?.Invoke();
                    });
                    break;
            }
        }

        /// <summary>
        /// Запускает эффект скрытия объекта на сцене.
        /// Альтернативный метод Hide. Вызовет Callback в любом случае
        /// </summary>
        public void Hide(Action onComplete = null)
        {
            _onComplete = onComplete;
            switch (State)
            {
                case ShowHideState.Hidden:
                    _onComplete?.Invoke();
                    break;
                case ShowHideState.Hide:
                    break;
                default:
                    Stop();
                    State = ShowHideState.Hide;
                    OnHide(() =>
                    {
                        SetObjectActive(false);
                        State = ShowHideState.Hidden;
                        _onComplete?.Invoke();
                    });
                    break;
            }
        }

        /// <summary>
        /// Мгновенно выполняет действие по показу объекта
        /// </summary>
        public void SetShown()
        {
            SetObjectActive(true);
            Stop();
            OnSetShown();
            State = ShowHideState.Shown;
        }

        /// <summary>
        /// Мгновенно выполняет действие по сокрытию объекта
        /// </summary>
        public void SetHidden()
        {
            Stop();
            OnSetHidden();
            SetObjectActive(false);
            State = ShowHideState.Hidden;
        }

        /// <summary>
        /// Останавливает все действия над объектом.
        /// </summary>
        public void Stop()
        {
            OnStop();
        }



        protected abstract void OnShow(Action onCompleted);

        protected abstract void OnHide(Action onCompleted);

        protected abstract void OnSetShown();

        protected abstract void OnSetHidden();

        protected abstract void OnStop();



        public void SetObjectActive(bool value)
        {
            if (_changeActive)
                gameObject.SetActive(value);
        }



        #region ContextMenu
        [ContextMenu("Test Show")]
        private void TestShow()
        {
            Show();
        }

        [ContextMenu("Test Hide")]
        private void TestHide()
        {
            Hide();
        }

        [ContextMenu("Test SetShown")]
        private void TestSetShown()
        {
            SetShown();
        }

        [ContextMenu("Test SetHidden")]
        private void TestSetHidden()
        {
            SetHidden();
        }
        #endregion
    }
}


///// <summary>
///// Базовый класс для контроля появления/скрытия объектов.
///// </summary>
//public abstract class BaseShowHide : MonoBehaviour
//{
//    private Action _onComplete;
//    [SerializeField]
//    protected ShowHideState _state;
//    [SerializeField]
//    protected bool _setOnStart = true;

//    public ShowHideState State
//    {
//        get { return _state; }
//        protected set { _state = value; }
//    }

//    public event Action<BaseShowHide, ShowHideState> StateChanged;

//    private void Start()
//    {
//        if (_setOnStart)
//            switch (State)
//            {
//                case ShowHideState.Show: Show(); break;
//                case ShowHideState.Shown: SetShown(); break;
//                case ShowHideState.Hide: Hide(); break;
//                case ShowHideState.Hidden: SetHidden(); break;
//            }
//    }

//    /// <summary>
//    /// Запускает эффект появления объекта на сцене
//    /// </summary>
//    public void Show(Action onCompleted = null)
//    {
//        if (State == ShowHideState.Show || State == ShowHideState.Shown)
//            return;

//        else
//        {
//            Stop();
//            ChangeState(ShowHideState.Show);
//            OnShow(() =>
//            {
//                ChangeState(ShowHideState.Shown);
//                onCompleted?.Invoke();
//            });
//        }
//    }

//    /// <summary>
//    /// Запускает эффект скрытия объекта на сцене
//    /// </summary>
//    public void Hide(Action onCompleted = null)
//    {
//        if (State == ShowHideState.Hide || State == ShowHideState.Hidden)
//            return;

//        else
//        {
//            Stop();
//            ChangeState(ShowHideState.Hide);
//            OnHide(() =>
//            {
//                ChangeState(ShowHideState.Hidden);
//                onCompleted?.Invoke();
//            });
//        }
//    }

//    /// <summary>
//    /// Мгновенно выполняет действие по показу объекта
//    /// </summary>
//    public void SetShown()
//    {
//        Stop();
//        ChangeState(ShowHideState.Shown);
//        OnSetShown();
//    }

//    /// <summary>
//    /// Мгновенно выполняет действие по сокрытию объекта
//    /// </summary>
//    public void SetHidden()
//    {
//        Stop();
//        ChangeState(ShowHideState.Hidden);
//        OnSetHidden();
//    }

//    /// <summary>
//    /// Останавливает все действия над объектом.
//    /// </summary>
//    public void Stop()
//    {
//        OnStop();
//    }

//    /// <summary>        
//    /// Запускает эффект появления объекта на сцене.
//    /// Альтернативный метод Show. Вызовет Callback в любом случае
//    /// </summary>
//    public void ShowAlter(Action onComplete = null)
//    {
//        _onComplete = onComplete;
//        switch (State)
//        {
//            case ShowHideState.Shown:
//                onComplete?.Invoke();
//                break;
//            case ShowHideState.Show:
//                break;
//            default:
//                Stop();
//                ChangeState(ShowHideState.Show);
//                OnShow(() =>
//                {
//                    ChangeState(ShowHideState.Shown);
//                    _onComplete?.Invoke();
//                });
//                break;
//        }
//    }

//    /// <summary>
//    /// Запускает эффект скрытия объекта на сцене.
//    /// Альтернативный метод Hide. Вызовет Callback в любом случае
//    /// </summary>
//    public void HideAlter(Action onComplete = null)
//    {
//        _onComplete = onComplete;
//        switch (State)
//        {
//            case ShowHideState.Hidden:
//                _onComplete?.Invoke();
//                break;
//            case ShowHideState.Hide:
//                break;
//            default:
//                Stop();
//                ChangeState(ShowHideState.Hide);
//                OnHide(() =>
//                {
//                    ChangeState(ShowHideState.Hidden);
//                    _onComplete?.Invoke();
//                });
//                break;
//        }
//    }

//    // Шаблонный метод для показа объекта
//    protected abstract void OnShow(Action onCompleted);

//    // Шаблонный метод для скрытия объекта
//    protected abstract void OnHide(Action onCompleted);

//    // Шаблонный метод для мгновенно показа объекта
//    protected abstract void OnSetShown();

//    // Шаблонный метод для мгновенно скрытия объекта
//    protected abstract void OnSetHidden();

//    // Шаблонный метод для остановки эффекта
//    protected abstract void OnStop();


//    private void ChangeState(ShowHideState state)
//    {
//        if (State == state)
//            return;
//        State = state;
//        StateChanged?.Invoke(this, state);
//    }

//    #region ContextMenu
//    [ContextMenu("Test Show")]
//    public void TestShow()
//    {
//        Show();
//    }

//    [ContextMenu("Test Hide")]
//    public void TestHide()
//    {
//        Hide();
//    }

//    [ContextMenu("Test SetShown")]
//    public void TestSetShown()
//    {
//        SetShown();
//    }

//    [ContextMenu("Test SetHidden")]
//    public void TestSetHidden()
//    {
//        SetHidden();
//    }
//    #endregion
//}
//}