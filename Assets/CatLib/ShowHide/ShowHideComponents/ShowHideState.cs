﻿
namespace ShowHideComponents
{
    public enum ShowHideState
    {
        Hidden = 0,
        Shown = 1,
        Hide = 2,
        Show = 3
    }
}