﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShowHideComponents
{
    public class ParticleSystemShowHide : BaseShowHide
    {
        private Action _onCompleted;
        [SerializeField] private ParticleSystemStopHandler[] _particleSystems;

        private void OnEnable()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].StopCallback += OnParticleSystemStopCallback;
        }

        private void OnDisable()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].StopCallback -= OnParticleSystemStopCallback;
        }

        private void Reset()
        {
            _particleSystems = GetComponentsInChildren<ParticleSystemStopHandler>();
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                var main = _particleSystems[i].ParticleSystem.main;
                main.playOnAwake = false;
            }
        }

        protected override void OnShow(Action onCompleted)
        {
            PlayParticles();
            onCompleted?.Invoke();
        }

        protected override void OnHide(Action onCompleted)
        {
            _onCompleted = onCompleted;
            StopParticles();
        }

        protected override void OnSetShown()
        {
            PlayPrewarmParticles();
        }

        protected override void OnSetHidden()
        {
            _onCompleted?.Invoke();
            StopAndClearParticles();
        }

        protected override void OnStop()
        {
        }


        private void PlayParticles()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].ParticleSystem.Play();
        }

        private void PlayPrewarmParticles()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                var ps = _particleSystems[i].ParticleSystem;
                ps.Stop();
                ps.Clear();
                var mainModule = ps.main;
                mainModule.prewarm = true;
                ps.Play();
                mainModule.prewarm = false;
            }
        }

        private void StopParticles()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].ParticleSystem.Stop();
        }

        private void StopAndClearParticles()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].ParticleSystem.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
        }

        private void OnParticleSystemStopCallback(ParticleSystemStopHandler obj)
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                if (_particleSystems[i].ParticleSystem.IsAlive())
                    break;
            _onCompleted?.Invoke();
        }
    }
}