﻿using System;
using UnityEngine;

namespace ShowHideComponents
{
    [RequireComponent(typeof(CanvasGroup))]
    public class CanvasGroupShowHide : BaseShowHide
    {
        private ProgressTimeRoutine _progressRoutine;
        private CanvasGroup _canvasGroup;

        [Header("Settings")]
        public AlphaShowHideSettings AlphaSettings;
        [Space]
        public bool UnscaledTime = false;
        public bool ChangeInteractable = false;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            _progressRoutine = new ProgressTimeRoutine(0f, AlphaSettings.ShowDuration, AlphaSettings.HideDuration, UnscaledTime, DeltaTimeType.Update, AlphaProgress);
        }

        protected override void OnShow(Action onCompleted)
        {
            _progressRoutine.Front(() =>
            {
                SetInteractable(true);
                onCompleted?.Invoke();
            });
        }

        protected override void OnHide(Action onCompleted)
        {
            SetInteractable(false);
            _progressRoutine.Back(onCompleted);
        }

        protected override void OnSetShown()
        {
            SetInteractable(true);
            _progressRoutine.SetEnd();
        }

        protected override void OnSetHidden()
        {
            _progressRoutine.SetStart();
            SetInteractable(false);
        }

        protected override void OnStop()
        {
            _progressRoutine.Stop();
        }

        private void SetInteractable(bool value)
        {
            if (ChangeInteractable)
                _canvasGroup.interactable = value;
        }

        private void AlphaProgress(float t)
        {
            _canvasGroup.alpha = AlphaSettings.AlphaCurve.Evaluate(t);
        }
    }

    [Serializable]
    public class AlphaShowHideSettings
    {
        public float ShowDuration = 1f;
        public float HideDuration = 1f;
        [CurveRange(1f, 1f)]
        public AnimationCurve AlphaCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
    }
}