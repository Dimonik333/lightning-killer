﻿using System;
using UnityEngine;

namespace ShowHideComponents
{
    public abstract class BaseShow : MonoBehaviour, IShow
    {
        private Action _onCompleted;

        [SerializeField] private ShowState _state;
        [SerializeField] private bool _setOnStart = true;
        [SerializeField] private bool _changeActive = true;

        public ShowState State
        {
            get { return _state; }
            protected set
            {
                if (_state == value)
                    return;
                _state = value;
                StateChanged?.Invoke(this, _state);
            }
        }

        public event Action<IShow, ShowState> StateChanged;

        private void Start()
        {
            if (_setOnStart && _onCompleted == null)
                switch (State)
                {
                    case ShowState.Show:
                        SetObjectActive(true);
                        Stop();
                        OnShowHide();
                        break;
                    case ShowState.Hidden:
                        Stop();
                        OnSetHidden();
                        SetObjectActive(false);
                        break;
                }
        }

        private void OnDestroy()
        {
            Stop();
        }

        /// <summary>
        /// Запускает эффект появления объекта на сцене
        /// </summary>
        public void ShowHide(Action onCompleted = null)
        {
            switch (State)
            {
                case ShowState.Show: break;
                case ShowState.Hidden:
                    _onCompleted = onCompleted;
                    SetObjectActive(true);
                    Stop();
                    State = ShowState.Show;
                    OnShowHide();
                    break;
            }
        }

        /// <summary>
        /// Мгновенно выполняет действие по сокрытию объекта
        /// </summary>
        public void SetHidden()
        {
            switch (State)
            {
                case ShowState.Show:
                    Stop();
                    OnSetHidden();
                    SetObjectActive(false);
                    State = ShowState.Hidden;
                    break;
                case ShowState.Hidden: break;
            }
        }

        /// <summary>
        /// Останавливает все действия над объектом.
        /// </summary>
        public void Stop()
        {
            OnStop();
        }

        protected void OnCompleted()
        {
            Stop();
            OnSetHidden();
            SetObjectActive(false);
            State = ShowState.Hidden;
            _onCompleted?.Invoke();
        }


        protected abstract void OnShowHide();

        protected abstract void OnSetHidden();

        protected abstract void OnStop();


        public void SetObjectActive(bool value)
        {
            if (_changeActive)
                gameObject.SetActive(value);
        }


        #region ContextMenu
        [ContextMenu("Test ShowHide")]
        private void TestShow()
        {
            ShowHide();
        }

        [ContextMenu("Test SetHidden")]
        private void TestSetHidden()
        {
            SetHidden();
        }
        #endregion
    }

    public interface IShow
    {
        ShowState State { get; }
        event Action<IShow, ShowState> StateChanged;

        void ShowHide(Action onCompleted = null);
        void SetHidden();
    }


    public enum ShowState
    {
        Hidden = 0,
        Show = 1

    }
}