﻿using UnityEngine;

namespace ShowHideComponents
{
    public class ParticleSystemShow : BaseShow
    {
        [SerializeField] private ParticleSystemStopHandler[] _particleSystems;

        private void OnEnable()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].StopCallback += OnParticleSystemStopCallback;
        }

        private void OnDisable()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].StopCallback -= OnParticleSystemStopCallback;
        }

        private void Reset()
        {
            _particleSystems = GetComponentsInChildren<ParticleSystemStopHandler>();
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                var main = _particleSystems[i].ParticleSystem.main;
                main.loop = false;
                main.playOnAwake = false;
            }
        }

        protected override void OnShowHide()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].ParticleSystem.Play();
        }

        protected override void OnSetHidden()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].ParticleSystem.Stop();
        }

        protected override void OnStop()
        {
        }

        private void OnParticleSystemStopCallback(ParticleSystemStopHandler obj)
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                if (_particleSystems[i].ParticleSystem.IsAlive(false))
                    return;
            OnCompleted();
        }
    }
}