﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Пул для хранения GameObject'ов
/// </summary>
public class PrefabPool
{
    private Scene _poolScene;
    private Stack<GameObject> _freeInstances;
    
    public int InstanceCount => _freeInstances.Count;

    public GameObject Prefab { get; private set; }
    public int ResizeValue { get; set; }

    /// <summary> </summary>
    /// <param name="prefab"> Оригинальный объект </param>
    /// <param name="poolScene"> Сцена/рут в которую будут добавляться объекты</param>
    /// <param name="initSize"> Начальный размер пула</param>
    /// <param name="resizeValue"> Размер увеличения пула</param>
    public PrefabPool(GameObject prefab, Scene poolScene, int initSize, int resizeValue)
    {
        Prefab = prefab;
        _poolScene = poolScene;
        ResizeValue = resizeValue;

        _freeInstances = new Stack<GameObject>(resizeValue);
        FillPool(initSize);
    }

    public void FillPool(int size)
    {
        for (int i = 0; i < size; i++)
        {
            var instance = UnityEngine.Object.Instantiate(Prefab);
            ReclaimInstance(instance);
        }
    }

    public GameObject GetObject()
    {
        if (_freeInstances.Count <= 0)
            FillPool(ResizeValue);
        var instance = _freeInstances.Pop();
        return instance;
    }

    public void ReclaimInstance(GameObject instance)
    {
        _freeInstances.Push(instance);
        instance.SetActive(false);
        var t = instance.transform;
        if (t.parent != null)
            instance.transform.SetParent(null);
        SceneManager.MoveGameObjectToScene(instance, _poolScene);
    }

    internal void Clear(bool destroyObjects = true)
    {
        if (destroyObjects)
            while (_freeInstances.Count > 0)
            {
                var instance = _freeInstances.Pop();
                GameObject.Destroy(instance);
            }
        _freeInstances.Clear();
    }
}