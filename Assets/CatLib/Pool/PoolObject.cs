﻿using UnityEngine;


public abstract class PoolObject : MonoBehaviour, IPoolComponent
{
    private IPoolComponent[] _poolComponents;
    public PrefabPool Pool { get; set; }

    public abstract void OnPoolComponentGet();
    public abstract void OnPoolComponentReclaim();

    public void OnPoolGet()
    {
        GetPoolComponents();
        for (int i = 0; i < _poolComponents.Length; i++)
            _poolComponents[i].OnPoolComponentGet();
    }

    public void OnPoolReclaim()
    {
        GetPoolComponents();
        for (int i = 0; i < _poolComponents.Length; i++)
            _poolComponents[i].OnPoolComponentReclaim();
    }

    private void GetPoolComponents()
    {
        if (_poolComponents == null || _poolComponents.Length == 0)
            _poolComponents = GetComponentsInChildren<IPoolComponent>();
    }
}

public interface IPoolComponent
{
    void OnPoolComponentGet();
    void OnPoolComponentReclaim();
}