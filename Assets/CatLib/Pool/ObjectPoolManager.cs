﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Менеджер пулов
/// </summary>
public class ObjectPoolManager : MonoSingleton<ObjectPoolManager>
{
    private static SceneController _poolSceneController = new SceneController("Object Pool");
    private Dictionary<GameObject, PrefabPool> _pools;


    public const int PoolResizeValue = 8;
    public const int PoolInitSize = 16;
    public Dictionary<GameObject, PrefabPool> Pools => _pools;
    protected override void OnInitialize()
    {
        _poolSceneController.UnloadScene();
        _poolSceneController.CreateScene();
        _pools = new Dictionary<GameObject, PrefabPool>();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }


    protected override void OnUninitialize()
    {
        _poolSceneController.UnloadScene();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
        => ClearAllPools();


    public static void FillPool(GameObject prefab, int poolInitSize = PoolInitSize, int poolResizeValue = PoolResizeValue)
    {
        Instance.Fill(prefab, poolInitSize, poolResizeValue);
    }

    public static void FillPool<T>(T prefab, int poolInitSize = PoolInitSize, int poolResizeValue = PoolResizeValue) where T : MonoBehaviour
    {
        Instance.Fill(prefab.gameObject, poolInitSize, poolResizeValue);
    }

    public static void Clear()
    {
        Instance.ClearAllPools();
    }


    public static GameObject Get(GameObject prefab)
    {
        return Instance.GetObject(prefab);
    }

    public static GameObject Get(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        return Instance.GetObject(prefab, position, rotation, parent);
    }

    public static T Get<T>(T prefab) where T : MonoBehaviour
    {
        return Instance.GetObject(prefab);
    }

    public static T Get<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent = null) where T : MonoBehaviour
    {
        return Instance.GetObject(prefab, position, rotation, parent);
    }


    public static void Reclaim(GameObject prefab, GameObject instance)
    {
        Instance.ReclaimObject(prefab, instance);
    }

    public static void Reclaim<T>(T prefab, T instance) where T : MonoBehaviour
    {
        Instance.ReclaimObject(prefab, instance);
    }

    public static T GetPoolObject<T>(T prefab) where T : PoolObject
    {
        return Instance.GetPoolObjectNew(prefab);
    }

    public static void ReclaimPoolObject<T>(T instance) where T : PoolObject
    {
        Instance.ReclaimPoolObjectNew(instance);
    }



    private void MoveToPoolPlace(GameObject go)
    {
        var t = go.GetComponent<Transform>();
        if (t == null)
            return;
        t.SetParent(null);
        _poolSceneController.MoveGameObjectToScene(go);
    }


    private PrefabPool GetOrCreatePool(GameObject prefab, int initSize = PoolInitSize, int resizeValue = PoolResizeValue)
    {
        if (_pools.ContainsKey(prefab))
            return _pools[prefab];
        else
        {
            var pool = new PrefabPool(prefab, _poolSceneController.GetScene(), initSize, resizeValue);
            _pools.Add(prefab, pool);
            return pool;
        }
    }

    private PrefabPool GetPool(GameObject prefab)
    {
        if (_pools.ContainsKey(prefab))
            return _pools[prefab];
        return null;
    }


    private void Fill(GameObject prefab, int initSize = PoolInitSize, int resizeValue = PoolResizeValue)
    {
        var pool = GetOrCreatePool(prefab, initSize, resizeValue);
        pool.FillPool(PoolInitSize);
    }

    public void ClearPool(GameObject prefab)
    {
        var pool = GetPool(prefab);
        if (pool != null)
            pool.Clear(true);
    }

    public void ClearAllPools()
    {
        _poolSceneController.UnloadScene();
        _poolSceneController.CreateScene();
        _pools.Clear();
    }


    public GameObject GetObject(GameObject prefab)
    {
        if (prefab == null)
            return null;
        return GetOrCreatePool(prefab).GetObject();
    }

    public GameObject GetObject(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        var go = GetObject(prefab);
        if (go)
        {
            var t = go.GetComponent<Transform>();
            if (parent != null)
                t.SetParent(parent);
            t.position = position;
            t.rotation = rotation;
        }
        return go;
    }

    public T GetObject<T>(T prefab) where T : MonoBehaviour
    {
        var go = GetObject(prefab.gameObject);
        return go ? go.GetComponent<T>() : null;
    }

    public T GetObject<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent = null) where T : MonoBehaviour
    {
        var go = GetObject(prefab.gameObject, position, rotation, parent).GetComponent<T>();
        return go ? go.GetComponent<T>() : null;
    }


    public void ReclaimObject(GameObject prefab, GameObject instance)
    {
        if (prefab == null || instance == null)
            return;
        var pool = GetOrCreatePool(prefab);
        //instance.SetActive(false);
        //MoveToPoolPlace(instance);
        pool.ReclaimInstance(instance);
    }

    public void ReclaimObject<T>(T prefab, T instance) where T : MonoBehaviour
    {
        ReclaimObject(prefab.gameObject, instance.gameObject);
    }


    #region IPoolObjects
    // Набор методов для работы с объектами, реализующими IPoolObject

    public T GetPoolObjectNew<T>(T prefab) where T : PoolObject
    {
        if (prefab == null)
            return null;
        var pool = GetOrCreatePool(prefab.gameObject);
        var go = pool.GetObject();
        if (go == null)
            return null;
        var instance = go.GetComponent<T>();

        instance.Pool = pool;
        instance.OnPoolGet();
        return instance;
    }

    public void ReclaimPoolObjectNew<T>(T instance) where T : PoolObject
    {
        if (instance == null)
            return;
        var pool = instance.Pool;
        instance.gameObject.SetActive(false);
        instance.OnPoolReclaim();
        pool.ReclaimInstance(instance.gameObject);
    }
    #endregion
}