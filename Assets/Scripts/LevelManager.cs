﻿using System;
using System.Collections;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private int _result;
    private uint? _timerRotuineId;

    [SerializeField] private CreatureSpawner _enemySpawner;
    [SerializeField] private CreatureSpawner _friendSpawner;
    [Space]
    [SerializeField] private LightningController _lightningController;
    [Space]
    [SerializeField] private IngameMenuLayer _ingameLayer;
    [SerializeField] private ResultMenuLayer _resultLayer;
    [Space]
    [SerializeField] private float _levelDuration;


    private void OnEnable()
    {
        _enemySpawner.CreatureKilled += OnEnemyCreatureKilled;
        _friendSpawner.CreatureKilled += OnFriendCreatureKilled;
    }

    private void OnDisable()
    {
        _enemySpawner.CreatureKilled -= OnEnemyCreatureKilled;
        _friendSpawner.CreatureKilled -= OnFriendCreatureKilled;
    }

    private void Start()
    {
        _ingameLayer.Show();
        _resultLayer.Hide();
        _timerRotuineId = CoroutineManager.Start(TimerRoutine(Stop));
    }

    private void OnEnemyCreatureKilled()
    {
        _result += 1;
        _ingameLayer.ShowResult(_result);
    }

    private void OnFriendCreatureKilled()
    {
        _result -= 3;
        _ingameLayer.ShowResult(_result);
    }

    public void Stop()
    {
        _enemySpawner.enabled = false;
        _friendSpawner.enabled = false;
        _lightningController.enabled = false;

        _resultLayer.Init(_result);
        _resultLayer.Show();

        _ingameLayer.Hide();
    }

    private IEnumerator TimerRoutine(Action onCompleted)
    {
        var leftTime = _levelDuration;
        while (leftTime > 0)
        {
            leftTime -= Time.deltaTime;
            _ingameLayer.ShowTime(leftTime);
            yield return null;
        }
        onCompleted?.Invoke();
    }
}
