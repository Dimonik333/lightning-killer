﻿using ShowHideComponents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingManager : ResourceSingleton<LoadingManager>
{
    [SerializeField] private BaseShowHide _backgroundEffect;

    public static void LoadScene(string scene)
        => Instance.FastLoadScene(scene);


    public void FastLoadScene(string scene)
    {
        gameObject.SetActive(true);
        _backgroundEffect.Show(() =>
            LoadSceneAsync(scene, () =>
                _backgroundEffect.Hide(() =>
                    gameObject.SetActive(false))));
    }

    private void LoadSceneAsync(string scene, Action onCompleted)
        => StartCoroutine(LoadSceneAsyncRoutine(scene, onCompleted));

    private IEnumerator LoadSceneAsyncRoutine(string scene, Action onComplete)
    {
        var op = SceneManager.LoadSceneAsync(scene);
        while (!op.isDone)
            yield return null;
        yield return null;
        onComplete?.Invoke();
    }
}
