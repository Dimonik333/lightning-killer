﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class IngameMenuLayer : MonoBehaviour
{
    [SerializeField] private Button _backButton;
    [SerializeField] private Text _timeText;
    [SerializeField] private Text _resultText;

    private void Awake()
    {
        _backButton.onClick.AddListener(OnBackClick);
    }

    private void OnBackClick()
        => LoadingManager.LoadScene("MainScene");

    private void OnEnable()
    {
        ShowTime(0);
        ShowResult(0);
    }


    public void ShowTime(float seconds)
    {
        var time = TimeSpan.FromSeconds(seconds).ToString("mm\\:ss\\.ff");
        _timeText.text = time;
    }

    public void ShowResult(int result)
        => _resultText.text = result.ToString();


    public void Show()
        => gameObject.SetActive(true);

    public void Hide()
        => gameObject.SetActive(false);
}
