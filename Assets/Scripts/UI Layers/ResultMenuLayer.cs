﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultMenuLayer : MonoBehaviour
{
    [SerializeField] Text _resultText;
    [SerializeField] Button _repearButton;
    [SerializeField] Button _menuButton;

    private void Awake()
    {
        _repearButton.onClick.AddListener(OnRepeatClick);
        _menuButton.onClick.AddListener(OnMenuClick);
    }


    private void OnMenuClick()
        => LoadingManager.LoadScene("MainScene");

    private void OnRepeatClick()
        => LoadingManager.LoadScene("LevelScene");


    public void Init(int result)
        =>
        _resultText.text = result.ToString();


    public void Show()
        => gameObject.SetActive(true);

    public void Hide()
        => gameObject.SetActive(false);
}


public class LevelTimer:MonoBehaviour
{
    
}