﻿using UnityEngine;
using UnityEngine.UI;

public class MainSceneMenu : MonoBehaviour
{
    [SerializeField] private Button _playButton;

    private void Awake()
    {
        _playButton.onClick.AddListener(OnPlayButton);
    }

    public void OnPlayButton()
    {
        LoadingManager.LoadScene("LevelScene");
    }
}
