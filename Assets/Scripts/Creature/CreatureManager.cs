﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CreatureManager : MonoSingleton<CreatureManager>
{
    private static SceneController _creatureSceneController = new SceneController("Creature Scene");

    protected override void OnInitialize()
    {
        _creatureSceneController.UnloadScene();
        _creatureSceneController.CreateScene();
        SceneManager.sceneLoaded += OnSceneManagerSceneLoaded;
    }

    protected override void OnUninitialize()
    {
        _creatureSceneController.UnloadScene();
        SceneManager.sceneLoaded -= OnSceneManagerSceneLoaded;
    }

    private void OnSceneManagerSceneLoaded(Scene arg0, LoadSceneMode arg1)
        => _creatureSceneController.CreateScene();


    public static Creature Create(Creature creaturePrefab, Vector3 position, Quaternion rotation, float health)
        => Instance.CreateCreature(creaturePrefab, position, rotation, health);
    public static void Reclaim(Creature creatureInstance)
        => Instance.ReclaimCreature(creatureInstance);

    public Creature CreateCreature(Creature creaturePrefab, Vector3 position, Quaternion rotation, float health)
    {
        var creatureInstance = ObjectPoolManager.GetPoolObject(creaturePrefab);
        creatureInstance.Init(health);

        var creatureTransform = creatureInstance.CachedTransform;
        creatureTransform.SetParent(null);
        creatureTransform.position = position;
        creatureTransform.rotation = rotation;

        _creatureSceneController.MoveGameObjectToScene(creatureInstance);
        return creatureInstance;
    }

    public void ReclaimCreature(Creature creatureInstance)
    {
        ObjectPoolManager.ReclaimPoolObject(creatureInstance);
    }
}