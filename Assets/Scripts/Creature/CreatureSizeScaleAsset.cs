﻿using UnityEngine;

[CreateAssetMenu(fileName = "Size Scale Data", menuName = "Create/Creature/Size Scale Data")]
public class CreatureSizeScaleAsset : ScriptableObject
{
    [CurveRange(1, 1)]
    [SerializeField] private AnimationCurve _sizeCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField] private float _minSize = 0f;
    [SerializeField] private float _maxSize = 1f;
    [Space]    
    [SerializeField] private float _minHealth = 1f;
    [SerializeField] private float _maxHealth = 2f;

    public float GetSizeByHealth(float health)
    {
        var t = Mathf.InverseLerp(_minHealth, _maxHealth, health);
        var size = Mathf.Lerp(_minSize, _maxSize, _sizeCurve.Evaluate(t));
        return size;
    }
}