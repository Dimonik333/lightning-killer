﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class CreatureSpawner : MonoBehaviour
{
    private uint? _checkRoutineId;
    private float _elapsedTime = 0f;
    private Rect _bounds;

    private List<Creature> _creaturesToDel = new List<Creature>();
    private HashSet<Creature> _creatures = new HashSet<Creature>();

    [SerializeField] private Creature _creaturePrefab;
    [Space]
    [SerializeField] private float _spawnDistance;
    [SerializeField] private float _reclaimDistance;
    [SerializeField] private Vector2 _boundsOffset = Vector2.one;
    [Space]
    [SerializeField] private float _spawnInterval = 1f;
    [SerializeField] private float _minHealth = 0.5f;
    [SerializeField] private float _maxHealth = 2f;

    public event Action CreatureKilled;


    private void Awake()
        => CalcBounds();

    private void OnValidate()
        => CalcBounds();

    private void OnEnable()
        => _checkRoutineId = CoroutineManager.Start(CheckRoutine());

    private void OnDisable()
    {
        CoroutineManager.Stop(_checkRoutineId);
        foreach (var creature in _creatures)
            ReclaimCreature(creature);
        _creatures.Clear();
    }


    private void Update()
    {
        if (_elapsedTime >= _spawnInterval)
        {
            SpawnCreature();
            _elapsedTime -= _spawnInterval;
        }
        _elapsedTime += Time.deltaTime;
    }

    public void SpawnCreature()
    {
        var position = transform.position + new Vector3(Random.Range(-_spawnDistance / 2, _spawnDistance / 2), 0, 0);
        var rotation = Quaternion.identity;
        var health = Random.Range(_minHealth, _maxHealth);

        var creature = CreatureManager.Create(_creaturePrefab, position, rotation, health);

        creature.Died += OnCreatureDied;
        creature.Destroyed += OnCreatureDestroyed;
        _creatures.Add(creature);

        creature.gameObject.SetActive(true);
    }

    private void ReclaimCreature(Creature creature)
    {
        creature.Died -= OnCreatureDied;
        creature.Destroyed -= OnCreatureDestroyed;

        CreatureManager.Reclaim(creature);
    }

    private void OnCreatureDied(Creature creature)
    {
        CreatureKilled?.Invoke();
    }

    private void OnCreatureDestroyed(Creature creature, bool reclaim)
    {
        if (reclaim)        
            ReclaimCreature(creature);        
        _creatures.Remove(creature);
    }

    private void CheckSpawnerBounds()
    {
        foreach (var creature in _creatures)
            if (!_bounds.Contains(creature.CachedTransform.position))
                _creaturesToDel.Add(creature);

        foreach (var creature in _creaturesToDel)
        {
            ReclaimCreature(creature);
            _creatures.Remove(creature);
        }

        _creaturesToDel.Clear();
    }



    private void CalcBounds()
    {
        var position = transform.position - new Vector3((_spawnDistance + _boundsOffset.x) / 2, _boundsOffset.y / 2, 0);
        _bounds = new Rect(position, new Vector2(_spawnDistance, _reclaimDistance) + _boundsOffset);
    }

    private IEnumerator CheckRoutine()
    {
        var yieldInstruction = new WaitForSeconds(1);
        while (true)
        {
            yield return yieldInstruction;
            CheckSpawnerBounds();
        }
    }
    private void OnDrawGizmosSelected()
    {
        var center = transform.position + transform.up * _reclaimDistance / 2;
        Gizmos.DrawWireCube(center, new Vector3(_spawnDistance, _reclaimDistance));

        CalcBounds();

        var color = Gizmos.color;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(_bounds.center, _bounds.size);
        Gizmos.color = color;
    }
}
