﻿using System;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float _maxValue;
    [SerializeField] private float _value;

    public float Value => _value;
    public float MaxValue => _maxValue;

    public event Action Empty;
    public event Action<float> ValueChanged;

    public void Init(float maxHealthValue)
    {
        _maxValue = maxHealthValue;
        _value = maxHealthValue;
    }

    public void TakeDamage(float damage)
    {
        var oldValue = _value;
        _value -= damage;
        
        if (_value == oldValue)
            return;

        ValueChanged.Invoke(_value);

        if (_value <= 0)
        {
            _value = 0f;
            Empty.Invoke();
        }
    }
}
