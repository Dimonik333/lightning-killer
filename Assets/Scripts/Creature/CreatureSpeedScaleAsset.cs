﻿using UnityEngine;

[CreateAssetMenu(fileName = "Speed Scale Data", menuName = "Create/Creature/Speed Scale Data")]
public class CreatureSpeedScaleAsset : ScriptableObject
{
    [CurveRange(1, 1)]
    [SerializeField] private AnimationCurve _speedCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField] private float _minSpeed = 1f;
    [SerializeField] private float _maxSpeed = 2f;
    [Space]
    [SerializeField] private float _minHealth = 1f;
    [SerializeField] private float _maxHealth = 2f;       

    public float GetSpeedByHealth(float health)
    {
        var t = Mathf.InverseLerp(_minHealth, _maxHealth, health);
        var speed = Mathf.Lerp(_minSpeed, _maxSpeed, _speedCurve.Evaluate(t));
        return speed;
    }
}
