﻿using ShowHideComponents;
using System;
using UnityEngine;

public class Creature : PoolObject
{
#pragma warning disable 649
    private Transform _transform;
    private bool _isAlive = true;
#pragma warning restore 649

    [SerializeField] private AbstractMovement _movement;
    [SerializeField] private Health _health;
    [SerializeField] private GameObject _body;
    [SerializeField] private BaseShow _destroyEffect;

    [SerializeField] private CreatureSpeedScaleAsset _speedData;
    [SerializeField] private CreatureSizeScaleAsset _sizeData;

    public event Action<Creature> Died;
    public event Action<Creature, bool> Destroyed;

    public Transform CachedTransform => _transform;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    private void OnEnable()
    {
        _health.ValueChanged += OnHealthValueChanged;
        _health.Empty += OnHealthEmpty;
    }

    private void OnDisable()
    {
        _health.ValueChanged -= OnHealthValueChanged;
        _health.Empty -= OnHealthEmpty;
    }

    private void OnDestroy()
    {
        _destroyEffect.SetHidden();
        Destroyed?.Invoke(this, false);
    }


    public void Init(float maxHealth)
    {
        _health.Init(maxHealth);
        OnHealthValueChanged(maxHealth);
    }

    private void OnHealthEmpty()
    {
        if (_isAlive)
        {
            _body.SetActive(false);
            _movement.enabled = false;
            _isAlive = false;

            Died?.Invoke(this);
            _destroyEffect.ShowHide(() => Destroyed?.Invoke(this, true));
        }
    }

    private void OnHealthValueChanged(float health)
    {
        var size = _sizeData.GetSizeByHealth(health);
        var speed = _speedData.GetSpeedByHealth(health);

        _transform.localScale = size * Vector3.one;
        _movement.Speed = speed;
    }


    public override void OnPoolComponentGet()
    {
        _isAlive = true;
        _body.gameObject.SetActive(true);
        _movement.enabled = true;

        _destroyEffect.SetHidden();
    }

    public override void OnPoolComponentReclaim() { }
}
