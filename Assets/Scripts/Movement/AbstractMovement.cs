﻿using UnityEngine;

public abstract class AbstractMovement : MonoBehaviour
{
    [SerializeField] protected float _monementSpeed;
    public virtual float Speed
    {
        get => _monementSpeed;
        set => _monementSpeed = value;
    }
}
