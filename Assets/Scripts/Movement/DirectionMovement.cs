﻿using UnityEngine;

public class DirectionMovement : AbstractMovement
{
    private Transform _transform;
    private Vector3 _movementDirection;

    private void Awake()
    {
        _transform = transform;
    }

    private void OnEnable()
    {
        _movementDirection = _transform.up;
    }

    private void Update()
    {
        _transform.position += (_monementSpeed * Time.deltaTime) * _movementDirection;
    }
}