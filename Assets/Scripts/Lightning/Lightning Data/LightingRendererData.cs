﻿using UnityEngine;

[CreateAssetMenu(fileName = "Lightning Render Data", menuName = "Lightning/RenderData")]
public class LightingRendererData : ScriptableObject
{
    [CurveRange(1, 1)]
    [SerializeField] private AnimationCurve _lightningWideCurve;
    [SerializeField] private float _widthMultiplier;
    [SerializeField] private float _lightningLengthLimit = 10f;

    public float WidthByLength(float length)
        => _widthMultiplier * _lightningWideCurve.Evaluate(length / _lightningLengthLimit);
}
