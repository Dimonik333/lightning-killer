﻿using UnityEngine;

[CreateAssetMenu(fileName = "Lightning Damager Data", menuName = "Lightning/DamagerData")]
public class LightningDamagerData : ScriptableObject
{
    [CurveRange(1, 1)]
    [SerializeField] private AnimationCurve _lightningDamageCurve;
    [SerializeField] private float _lightningDamageMultiplier = 1f;
    [SerializeField] private float _lightningLengthLimit = 10f;

    public float DamageMultiplierByLength(float length)
        => _lightningDamageMultiplier * _lightningDamageCurve.Evaluate(length / _lightningLengthLimit);

}
