﻿using ShowHideComponents;
using UnityEngine;

public class LightningBallState : AbstractLightningState
{
#pragma warning disable 649
    private BaseShowHide _lightningBall = null;
    private Transform _lightningBallTransform = null;
#pragma warning restore 649

    public LightningBallState(Lightning lightning) : base(lightning) { }

    public override void SetPositions(Vector2[] points)
    {
        var pointsCount = points.Length;
        if (pointsCount == 0)
            _lightning.SetState(LightningState.Idle);
        else if (pointsCount > 1)
            _lightning.SetState(LightningState.LigtningBolt);
    }

    public override void OnStateEnter()
    {
        var point = _lightning.Positions[0];
        _lightningBall = ObjectPoolManager.Get(_lightning.LightningBallPrefab, point, Quaternion.identity, _lightning.transform);
        _lightningBallTransform = _lightningBall.GetComponent<Transform>();
        _lightningBall.Show();
    }

    public override void OnStateExit()
    {
        var lb = _lightningBall;
        _lightningBall = null;
        lb.Hide(() => ObjectPoolManager.Reclaim(_lightning.LightningBallPrefab, lb));
    }

    public override void Update()
    {
        _lightningBallTransform.position = _lightning.Positions[0];
    }
}
