﻿using UnityEngine;

public class IdleLightningState : AbstractLightningState
{
    public IdleLightningState(Lightning lightning) : base(lightning) { }

    public override void SetPositions(Vector2[] points)
    {
        var pointsCount = points.Length;
        if (pointsCount == 1)
            _lightning.SetState(LightningState.LigtningBall);
        if (pointsCount > 1)
            _lightning.SetState(LightningState.LigtningBolt);
    }

    public override void Update()
    {
    }
}
