﻿using UnityEngine;

public abstract class AbstractLightningState
{
    protected Lightning _lightning;

    public AbstractLightningState(Lightning lightning)
    {
        _lightning = lightning;
    }

    public abstract void SetPositions(Vector2[] points);
    public abstract void Update();

    public virtual void OnStateEnter() { }
    public virtual void OnStateExit() { }
}
