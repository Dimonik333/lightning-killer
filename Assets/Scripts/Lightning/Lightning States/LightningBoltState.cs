﻿using UnityEngine;

public class LightningBoltState : AbstractLightningState
{
    private LightningRenderer _lightningRenderer;

    public LightningBoltState(Lightning lightning) : base(lightning) { }


    public override void OnStateEnter()
    {
        var position = _lightning.CachedTransform.position;
        _lightningRenderer = ObjectPoolManager.Get(_lightning.LightningRendererPrefab, position, Quaternion.identity, _lightning.CachedTransform);
        _lightningRenderer.Show();
        _lightning.LightningDamager.enabled = true;
        Update();
    }

    public override void OnStateExit()
    {
        _lightning.LightningDamager.enabled = false;
        _lightningRenderer.Hide(() => ObjectPoolManager.Reclaim(_lightning.LightningRendererPrefab, _lightningRenderer));
    }

    public override void SetPositions(Vector2[] points)
    {
        var pointsCount = points.Length;
        if (pointsCount == 0)
            _lightning.SetState(LightningState.Idle);
        else if (pointsCount == 1)
            _lightning.SetState(LightningState.LigtningBall);
    }

    public override void Update()
    {
        var startPoint = _lightning.Positions[0];
        var endPoint = _lightning.Positions[1];

        _lightningRenderer.SetPoints(startPoint, endPoint);
        _lightning.LightningDamager.SetPoints(startPoint, endPoint);
    }
}