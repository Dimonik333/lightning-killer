﻿using ShowHideComponents;
using UnityEngine;

public class LightningController : MonoBehaviour
{
#pragma warning disable 649
    private Transform _cameraTransform;
    [SerializeField] private Lightning _lightning;
#pragma warning restore 649

    private void Start()
    {
        _cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        var positions = new Vector2[Input.touchCount];

        for (int i = 0; i < Input.touchCount; i++)
        {
            positions[i] = GetTouchPosition(Input.GetTouch(i));
            Debug.DrawLine(Vector3.zero, positions[i]);
        }
        _lightning.SetPositions(positions);

        //var position = new Vector2[2];
        //position[0] = Vector3.zero;
        //if (Input.GetMouseButton(0))
        //{
        //    var mousePosition = Input.mousePosition;
        //    mousePosition.z = -Camera.main.transform.position.z;
        //    position[1] = Camera.main.ScreenToWorldPoint(mousePosition);
        //    _lightning.SetPositions(position);
        //}
        //else
        //    _lightning.SetPositions(new Vector2[] { });
    }

    private void OnDisable()
    {
        _lightning.SetPositions(new Vector2[] { });
    }

    private Vector2 GetTouchPosition(Touch touch)
    {
        Vector3 position = touch.position;
        position.z = -_cameraTransform.position.z;
        var touchPosition = Camera.main.ScreenToWorldPoint(position);

        return touchPosition;
    }
}
