﻿using UnityEngine;

public class LightningDamager : MonoBehaviour
{
    private Vector3 _startPoint;
    private Vector3 _endPoint;

    [SerializeField] private LightningDamagerData _damageData;
    public void SetPoints(Vector3 startPoint, Vector3 endPoint)
    {
        _startPoint = startPoint;
        _endPoint = endPoint;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        var direction = _endPoint - _startPoint;
        var distance = direction.magnitude;
        var hits = Physics2D.RaycastAll(_startPoint, direction, distance);

        var damageMultiplier = _damageData.DamageMultiplierByLength(distance);
        var damage = damageMultiplier * Time.deltaTime;

        for (int i = 0; i < hits.Length; i++)
        {
            var health = hits[i].transform.GetComponent<Health>();
            health?.TakeDamage(damage);
        }
    }
}
