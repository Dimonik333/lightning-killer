﻿using ShowHideComponents;
using System;
using UnityEngine;

public class LightningRenderer : BaseShowHide
{
    public const int PointsCount = 2;

    private Vector3[] _points = new Vector3[PointsCount] { Vector3.zero, new Vector3(1, 0, 0) };
    private Vector3[] _directions = new Vector3[PointsCount] { Vector3.up, Vector3.up };
    private float _lightningLength = 1f;

    private ShowHideGroup _pointsEffectsGroup;

    private Transform _transform;
    private Transform[] _pointsEffectsTransforms = new Transform[PointsCount];

    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private BaseShowHide[] _pointsEffects;
    [SerializeField] private LightingRendererData _lightningData;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
        for (int i = 0; i < _pointsEffects.Length; i++)
            _pointsEffectsTransforms[i] = _pointsEffects[i].GetComponent<Transform>();
        _pointsEffectsGroup = new ShowHideGroup(_pointsEffects);
    }


    public void SetPoints(Vector3 startPoint, Vector3 endPoint)
    {
        _points[0] = startPoint;
        _points[1] = endPoint;

        _directions[0] = endPoint - startPoint;
        _directions[1] = startPoint - endPoint;

        _lightningLength = Vector2.Distance(startPoint, endPoint);
    }

    public void Update()
    {
        UpdatePointsEffects();
        UpdateLine();
    }

    private void UpdatePointsEffects()
    {
        for (int i = 0; i < PointsCount; i++)
        {
            _pointsEffectsTransforms[i].position = _points[i];
            _pointsEffectsTransforms[i].rotation = Quaternion.LookRotation(_directions[i], Vector3.forward);
        }
    }

    private void UpdateLine()
    {
        _lineRenderer.SetPositions(_points);
        _lineRenderer.widthMultiplier = _lightningData.WidthByLength(_lightningLength);
    }


    protected override void OnShow(Action onCompleted)
    {
        _lineRenderer.enabled = true;
        _pointsEffectsGroup.Show(onCompleted);

        UpdatePointsEffects();
        UpdateLine();
    }

    protected override void OnHide(Action onCompleted)
    {
        _lineRenderer.enabled = false;
        _pointsEffectsGroup.Hide(onCompleted);
    }


    protected override void OnSetShown()
    {
        _lineRenderer.enabled = true;
        _pointsEffectsGroup.SetShown();
    }

    protected override void OnSetHidden()
    {
        _lineRenderer.enabled = false;
        _pointsEffectsGroup.SetHidden();
    }


    protected override void OnStop()
    {
        _pointsEffectsGroup.Stop();
    }
}
