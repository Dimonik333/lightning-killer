﻿using ShowHideComponents;
using UnityEngine;

public class Lightning : MonoBehaviour
{
#pragma warning disable
    private AbstractLightningState _currentState;

    private AbstractLightningState _idleState;
    private AbstractLightningState _ballState;
    private AbstractLightningState _boltState;
#pragma warning restore


    public Vector2[] Positions;
    public BaseShowHide LightningBallPrefab;
    public LightningRenderer LightningRendererPrefab;
    public LightningDamager LightningDamager;
    public Transform CachedTransform { get; private set; }


    private void Awake()
    {
        CachedTransform = GetComponent<Transform>();

        _idleState = new IdleLightningState(this);
        _ballState = new LightningBallState(this);
        _boltState = new LightningBoltState(this);
    }

    private void Start()
    {
        LightningDamager.enabled = false;
        _currentState = _idleState;
        _currentState.OnStateEnter();
    }

    public void SetState(LightningState state)
    {
        _currentState.OnStateExit();
        switch (state)
        {
            case LightningState.Idle: _currentState = _idleState; break;
            case LightningState.LigtningBall: _currentState = _ballState; break;
            case LightningState.LigtningBolt: _currentState = _boltState; break;
        }
        _currentState.OnStateEnter();
    }

    public void SetPositions(params Vector2[] positions)
    {
        Positions = positions;
        _currentState.SetPositions(positions);
    }

    private void Update()
    {
        _currentState.Update();
    }
}
